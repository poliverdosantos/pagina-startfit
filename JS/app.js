const wrapper = document.querySelector(".sliderWrapper");
const menuItems = document.querySelectorAll(".menuItem");

const products = [
  {
    id: 1,
    title: "STARTFIT - BOLIVIA",
    colors: [
      {
        img: "./img/air.png",
      },
    ],
  },

  {
    id: 2,
    title: "Parfait con yogurt griego",
    price: 10,
    colors: [
      {
        img: "./img/jordan.png",
      },
    ],
  },

  {
    id: 3,
    title: "Barristas proteicas",
    price: 10,
    colors: [
      {
        img: "./img/blazer.png",
      },
    ],
  },
  {
    id: 4,
    title: "Frutos secos",
    price: 6,
    colors: [
      {
        img: "./img/crater.png",
      },
    ],
  },
  {
    id: 5,
    title: "SOMOS:",
    price:0,
    colors: [
      {
        img: "./img/hippie.png",
      },
    ],
  },
];

let choosenProduct = products[0];

const currentProductImg = document.querySelector(".productImg");
const currentProductTitle = document.querySelector(".productTitle");
const currentProductPrice = document.querySelector(".productPrice");
const currentProductColors = document.querySelectorAll(".color");
const currentProductSizes = document.querySelectorAll(".size");

//controla el menu de navegacion con efectos 
menuItems.forEach((item, index) => {
  item.addEventListener("click", () => {
    //cambiar la diapositiva actual
    wrapper.style.transform = `translateX(${-100 * index}vw)`;

    //cambiar el producto elegido
    choosenProduct = products[index];

    // cambiar textos de producto actual
    currentProductTitle.textContent = choosenProduct.title;
    currentProductPrice.textContent = "$" + choosenProduct.price;
    currentProductImg.src = choosenProduct.colors[0].img;
    
    //asignando nuevos colores
    currentProductColors.forEach((color, index) => {
      color.style.backgroundColor = choosenProduct.colors[index].code;
    });
  });
});

currentProductColors.forEach((color, index) => {
  color.addEventListener("click", () => {
    currentProductImg.src = choosenProduct.colors[index].img;
  });
});

currentProductSizes.forEach((size, index) => {
  size.addEventListener("click", () => {
    currentProductSizes.forEach((size) => {
      size.style.backgroundColor = "white";
      size.style.color = "black";
    });
    size.style.backgroundColor = "black";
    size.style.color = "white";
  });
});

const productButton = document.querySelector(".productButton");
const payment = document.querySelector(".payment");
const close = document.querySelector(".close");

productButton.addEventListener("click", () => {
  payment.style.display = "flex";
});

close.addEventListener("click", () => {
  payment.style.display = "none";
});


